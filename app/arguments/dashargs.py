from argparse import ArgumentParser

class DashArguments():

    argument_parser = None

    def __init__(self):
        self.argument_parser = ArgumentParser()


    def parse_arguments(self):
        self.argument_parser.add_argument('-d',
            action = "store_true",
            dest = 'daemonize',
            help = "Run the server as a daemon")
        self.argument_parser.add_argument('--port',
            dest = 'port',
            default = None,
            help = "Force webinterface to listen on this port")
        options = self.argument_parser.parse_args()
        return options