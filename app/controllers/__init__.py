import cherrypy

import json

def base():
    base_str = cherrypy.config.get('config').get('global', 'urlBase')
    if base_str:
        return '/' + base_str.strip('/')
    return ''

class BaseController:

    def __init__(self):
        self.globals = {}


    def render(self, params):
        self.update_globals()
        params.update(self.globals)
        return params

    def update_globals(self):
        self.globals['baseUrl'] = base() + '/'

    def post_success_with_data(self, data):
        json_dict = dict()
        json_dict['success'] = True
        json_dict['data'] = data
        json_dict['errors'] = []
        return json.dumps(json_dict)

    def post_failure_with_errors(self, errors):
        json = "{"
        json += '"success": false, '
        json += '"data": null, '
        json += '"errors": ['
        for error in errors:
            json += '"'
            json += error
            json += '"'
            if errors[-1] != error:
                json += ', '
        json += ']'
        json += '}'
        return json
    