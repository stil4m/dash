import cherrypy

from jinja2 import Environment, FileSystemLoader
from app.lib.plugins.pluginmanager import PluginManager

class HomeController:

    env = Environment(loader=FileSystemLoader('templates'))

    @cherrypy.expose
    def index(self):
        tmpl = self.env.get_template('index.html')
        plugs = PluginManager.shared_instance().all_plugins()
        return tmpl.render(plugins=plugs)


    def restart(self):
        cherrypy.engine.restart()

    def plugins(self):
        template = self.env.get_template('plugins.html')
        plugs = PluginManager.shared_instance().all_plugins()
        return template.render(plugins=plugs)

        