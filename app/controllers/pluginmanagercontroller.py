import cherrypy
from . import BaseController
from jinja2 import Environment, FileSystemLoader

from app.lib.plugins.pluginmanager import PluginManager
from app.lib.plugins.pluginrepository import PluginRepository

class PluginManagerController(BaseController):

    def __init__(self):
        BaseController.__init__(self)
        self.env = Environment(loader=FileSystemLoader('templates/pluginmanager'))
        self.repository = PluginRepository()
        self.setup

    def index(self):
        template = self.env.get_template('index.html')
        plugin_manager = PluginManager.shared_instance()
        plugins = plugin_manager.all_plugins()
        installed_plugins = plugin_manager.all_installed_plugins()
        available_plugins = self.repository.get_available_plugins()
        print installed_plugins
        return template.render(
            plugins=plugins,
            available_plugins=available_plugins,
            installed_plugins = installed_plugins
        )

    def setup(self, plugin_name):
        template = self.env.get_template('setup.html')
        print plugin_name
        return template.render(plugin_name=plugin_name)

    def active_plugins(self):
        template = self.env.get_template('activeplugins.html')
        plugins = PluginManager.shared_instance().all_plugins()
        return template.render(plugins=plugins)

    def available_plugins(self):
        template = self.env.get_template('availableplugins.html')
        available_plugins = self.repository.get_available_plugins()
        return template.render(available_plugins=available_plugins)

    def installed_plugins(self):
        template = self.env.get_template('installedplugins.html')
        installed_plugins = PluginManager.shared_instance().all_installed_plugins()
        return template.render(installed_plugins=installed_plugins)

    def install(self, plugin_name, working_name):
        plugin_manager = PluginManager.shared_instance()
        error = plugin_manager.plugin_name_is_allowed(working_name)
        if error is not None:
            errors = []
            errors.append(error)
            return self.post_failure_with_errors(errors)
        available_plugins = self.repository.get_available_plugins()
        repo = available_plugins[plugin_name]
        plugin_manager.install_plugin(
            working_name,
            plugin_name,
            repo
        )
        return self.post_success_with_data([])

    def delete_plugin(self, plugin_name):
        print "DELETE PLUGIN " + plugin_name
        PluginManager.shared_instance().delete_plugin(plugin_name)