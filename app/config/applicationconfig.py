import ConfigParser
from app.config.applicationdefaultconfig import DefaultConfig

class AppConfig:
    bool = {'true':True, 'false':False}
    
    def __init__(self, config_file_path):
        self.config_file_path = config_file_path
        self.config_parser = ConfigParser.RawConfigParser()
        self.config_parser.read(config_file_path)
        default_config = DefaultConfig()
        default_config.setup_default_config(self)

        self.init_config()
        self.save()
        
    def parser(self):
        return self.config_parser
    
    def set(self, section, option, value):
        return self.config_parser.set(section, option, value)

    def get(self, section, option):
        value = self.config_parser.get(section, option)
        if str(value).lower() in self.bool:
            return self.bool.get(str(value).lower())
        return value if type(value) != str else value.strip()
            
    def add_section(self, section):
        if not self.config_parser.has_section(section):
            self.config_parser.add_section(section)
            
    def set_default(self, section, option, value):
        if not self.config_parser.has_option(section, option):
            self.config_parser.set(section, option, value)

    def save(self):
        with open(self.config_file_path, 'wb') as configfile:
            self.config_parser.write(configfile)

    def init_config(self):
        self.set_default('global', 'urlBase', '')
    