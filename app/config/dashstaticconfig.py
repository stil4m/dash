__author__ = 'mstijlaart'

from app.config.routes import setup as Routes

class DashStaticConfig(dict):

    path_base = None

    def __init__(self, path_base, application_launch_context):
        dict.__init__(self)
        self.path_base = path_base
        self['/'] = {
            'request.dispatch': Routes(),
            'tools.sessions.on': True,
            'tools.sessions.timeout': 240,
            'tools.gzip.on': True,
            'tools.gzip.mime_types':
                ['text/html',
                 'text/plain',
                 'text/css',
                 'text/javascript',
                 'application/javascript'
                ]
        }

        self['/media'] = {
            'tools.staticdir.on': True,
            'tools.staticdir.root': path_base,
            'tools.staticdir.dir': "media",
            'tools.expires.on': True,
            'tools.expires.secs': 3600 * 24 * 7
        }
        self['/cache'] = {
            'tools.staticdir.on': True,
            'tools.staticdir.root': application_launch_context.data_dir,
            'tools.staticdir.dir': "cache",
            'tools.expires.on': True,
            'tools.expires.secs': 3600 * 24 * 7
        }

    def append_static_dir(self, static_dir_path,
                                           internal_static_dir):
        self[static_dir_path] = {
            'tools.staticdir.on': True,
            'tools.staticdir.root': self.path_base + static_dir_path,
            'tools.staticdir.dir': internal_static_dir,
            'tools.expires.on': True,
            'tools.expires.secs': 3600 * 24 * 7
        }