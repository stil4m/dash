
from app.controllers.home import HomeController
from app.controllers.pluginmanagercontroller import PluginManagerController
from app.lib.dashroutesdispatcher import DashRoutesDispatcher

def setup():
    mapper = DashRoutesDispatcher.shared_instance().get_dispatcher()
    mapper.minimization = False
    mapper.explicit = False
    mapper.append_slash = True

    plugin_manager_controller = PluginManagerController()
    home_controller = HomeController()

    mapper.connect(
        name='Home',
        route='/',
        controller=home_controller,
        action = 'index'
    )

    mapper.connect(
        name='Home',
        route='/system/restart',
        controller=home_controller,
        action = 'restart')


    mapper.connect(
        name='Home',
        route='/home',
        controller=home_controller,
        action = 'index'
    )

    mapper.connect(
        name='Home',
        route='/plugins',
        controller=home_controller,
        action = 'plugins'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager",
        controller=plugin_manager_controller,
        action = 'index'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/setup",
        controller=plugin_manager_controller,
        action = 'setup'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/availablePlugins",
        controller=plugin_manager_controller,
        action = 'available_plugins'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/activePlugins",
        controller=plugin_manager_controller,
        action = 'active_plugins'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/install",
        controller=plugin_manager_controller,
        action = 'install'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/installedPlugins",
        controller=plugin_manager_controller,
        action = 'installed_plugins'
    )

    mapper.connect(
        name="PluginManager",
        route="/pluginmanager/deletePlugin",
        controller=plugin_manager_controller,
        action = 'delete_plugin'
    )
    return mapper