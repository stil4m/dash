import cherrypy as cp

class CheetahHandler(cp.dispatch.LateParamPageHandler):
    def __init__(self, template, group, next_handler, is_plugin):
        cp.dispatch.LateParamPageHandler.__init__(
            self,
            template,
            group,
            next_handler)
        self.template = template
        self.next_handler = next_handler
        self.group = group
        self.is_plugin = is_plugin
    
    def __call__(self):
        env = globals().copy()
        env.update(self.next_handler())

        mod = 'app.views.'
        if self.is_plugin:
            mod = 'plugins.'

        if self.group is not None:
            mod += (self.group + '.')
        tmpl = getattr(
            __import__(
                mod+self.template,
                globals(),
                locals(),
                [self.template],
                -1
            ),
            self.template)
        return str(tmpl(searchList=[env]))

class CheetahLoader(object):
    def __call__(self, template, group=None, plugin=False):
        cp.request.handler = CheetahHandler(
            template,
            group,
            cp.request.handler,
            plugin
        )

CHEETAH_LOADER = CheetahLoader()
cp.tools.cheetah = cp.Tool('on_start_resource', CHEETAH_LOADER)