class DefaultConfig:
    
    def setup_default_config(self, app_config):
        app_config.add_section('global')
        app_config.set_default('global', 'server.environment', 'production')
        app_config.set_default('global', 'host', '0.0.0.0')
        app_config.set_default('global', 'port', 5000)
        app_config.set_default('global', 'username', '')
        app_config.set_default('global', 'password', '')
        app_config.save()