import os

class ApplicationLaunchContext():
    def __init__(self, data_dir):
        self.config_path = None
        self.data_dir = os.path.abspath(data_dir)
        
    def config_file_path(self):
        cfp = None
        if self.config_path:
            cfp = self.config_path
        else:
            cfp = os.path.join(self.data_dir, 'config.ini')
        return os.path.abspath(cfp)
        

    def validate_config_file_path(self):
        tar = self.config_file_path()
        if not os.access(os.path.dirname(tar), os.W_OK) and \
           not os.access(tar, os.W_OK):
            if not os.path.exists(os.path.dirname(tar)):
                os.makedirs(os.path.dirname(tar))
            else:
                raise SystemExit("Directory for config file must be writeable")

        
    def validate_data_dir(self):
        if not os.access(self.data_dir, os.W_OK):
            raise SystemExit("Data dir must be writeable '" +
                             self.data_dir + "'")
        
    def validate(self):
        self.validate_data_dir()
        self.validate_config_file_path()