__author__ = 'mstijlaart'

from app.lib.cron.dashcronjob import DashCronJob

from cherrypy.process import plugins

class DashProcess(plugins.SimplePlugin):

    config = {}
    threads = {}

    def __init__(self, bus, config, debug):
        plugins.SimplePlugin.__init__(self, bus)
        self.config = config
        self.debug = debug

    def start(self):
        print "Starting Dash Process"
        for key, thread in self.threads.iteritems():
            thread.start()

    def stop(self):
        print "Stopping Dash Process"
        for key, thread in self.threads.iteritems():
            thread.quit()

        for key, thread in self.threads.iteritems():
            thread.join()

    def append_method_and_timeout(self, thread_name, method, timeout):
        thread = DashCronJob(method, timeout)
        self.threads[thread_name] = thread