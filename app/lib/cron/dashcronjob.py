import threading
import time

class DashCronJob(threading.Thread):

    last_checked = 0
    abort = False
    running = False

    def __init__(self, method, timeout, wait=1):
        self.wait = wait
        threading.Thread.__init__(self)
        self.timeout = timeout
        self.target_method = method

    def is_running(self):
        return self.running

    def quit(self):
        self.abort = True

    def can_shutdown(self):
        return (self.abort and not self.running)

    def sleep(self):
        time.sleep(self.wait)

    def run(self):
        self.target_method()
        self.last_checked = time.time()

        self.sleep()
        while True and not self.abort:
            now = time.time()

            if (self.last_checked + self.timeout) < now:
                self.target_method()
                self.last_checked = now
            self.sleep()