from git.exceptions import GitCommandFailedException
from git.repository import LocalRepository

__author__ = 'mstijlaart'

import threading

class PluginInstallThread(threading.Thread):

    def __init__(self, installing_plugin):
        threading.Thread.__init__(self)
        self.installing_plugin = installing_plugin

    def run(self):
        repo = LocalRepository('plugins/' + self.installing_plugin.working_name)
        try:
            repo.clone(self.installing_plugin.repository)
            self.installing_plugin.set_success_state()

        except GitCommandFailedException:
            self.installing_plugin.set_failed_state()


