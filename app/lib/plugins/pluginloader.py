from app.lib.plugins.plugin import Plugin

import  os

class PluginLoaderError(Exception):
    def __init__(self):
        Exception.__init__(self)
        print "Init Plugin Loader Error"
        
class PluginLoader:
    def load_plugin(self, plug_file):
        if not self.check_executable_path(plug_file):
            raise PluginLoaderError(
                'PluginLoaderError',
                'Plugin has no valid executable path'
            )
            
        return Plugin(plug_file)
        
    def check_executable_path(self, path):
        return os.path.isfile(self.get_executable_path(path))
        
    def get_executable_path(self, plug_path):
        return (plug_path + '/brick.py')
        