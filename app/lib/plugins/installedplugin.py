__author__ = 'mstijlaart'

class InstalledPlugin:

    def __init__(self, working_name, plugin_name, repository):
        self.working_name = working_name
        self.plugin_name = plugin_name
        self.repository = repository
        self.state = "PreInstall"

    def get_plugin_name(self):
        return self.plugin_name

    def get_working_name(self):
        return self.working_name

    def set_failed_state(self):
        self.state = "Failed"

    def set_success_state(self):
        self.state = "Succeeded"



