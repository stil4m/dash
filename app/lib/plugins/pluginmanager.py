import  os, glob

from app.lib.singleton import Singleton
from app.lib.plugins.pluginloader import PluginLoader, PluginLoaderError

from .installedplugin import InstalledPlugin
from .plugininstallthread import PluginInstallThread

PLUGIN_PATH = os.getcwd() + '/plugins/'

import shutil

def containsAny(str, set):
    return 1 in [c in str for c in set]

@Singleton
class PluginManager(object):
    
    def __init__(self):
        self.plugins = self.get_plugins()
        self.installed_plugins = []

    def setup_dash_process(self, dash_process):
        self.schedule_plugins(dash_process)

    def get_plugins(self):
        result = []
        plugin_loader = PluginLoader()
        for path in self.all_files_in_plugin_dir():
            if os.path.isdir(path):
                try:
                    plugin = plugin_loader.load_plugin(path)
                    result.append(plugin)
                except PluginLoaderError:
                    print "PluginManager - Could not load plugin: " + path
        return result
    
    def all_files_in_plugin_dir(self):
        pwd = os.path.join(PLUGIN_PATH, '*')
        return glob.glob(pwd)

    def all_plugins(self):
        return self.plugins

    def all_installed_plugins(self):
        return self.installed_plugins

    def install_plugin(self, working_name, plugin_name, repository):
        plug = InstalledPlugin(working_name=working_name, plugin_name=plugin_name, repository=repository)
        thread = PluginInstallThread(plug)
        thread.start()
        self.installed_plugins.append(plug)


    def schedule_plugins(self, dash_process):
        for plugin in self.plugins:
            dash_process.append_method_and_timeout(
                plugin.get_name(),
                plugin.update,
                plugin.update_timeout()
            )

    def update_config_with_plugins(self, config):
        for plugin in self.plugins:
            config.append_static_dir("/plugins/" + plugin.get_name(), "media")

    def plugin_name_is_allowed(self, working_name):
        if len(working_name) == 0:
            return 'Plugin name may not be an empty string.'
        if working_name[0].isdigit():
            return 'Plugin name may not start with an digit.'
        if containsAny(working_name, "\"',.&()[]{}-+/\\*%!$#@=|`~<>?"):
            return 'Plugin name includes invalid character.'
        for active in self.plugins:
            if active.get_name() == working_name:
                return 'Plugin name is taken.'
        for installed in self.installed_plugins:
            if installed.get_working_name() == working_name:
                return 'Plugin name is taken.'
        return None

    def delete_plugin(self, delete_plugin_name):
        for plugin in self.plugins:
            if plugin.name == delete_plugin_name:
                self.plugins.remove(plugin)
                shutil.rmtree(PLUGIN_PATH + delete_plugin_name)
