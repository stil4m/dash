__author__ = 'mstijlaart'

class PluginRepository:

    def __init__(self):
        self.available_plugins = {}
        self.available_plugins['dash-brick-9gag'] = 'https://bitbucket.org/stil4m/dash-brick-9gag.git'
        self.available_plugins['dash-brick-myserver'] = 'https://bitbucket.org/stil4m/dash-brick-myserver.git'
        self.available_plugins['dash-brick-sickbeard'] = 'https://bitbucket.org/stil4m/dash-brick-sickbeard.git'
        self.available_plugins['dash-brick-twitter'] = 'https://bitbucket.org/stil4m/dash-brick-twitter.git'
        self.available_plugins['dash-brick-feedburner'] = 'https://bitbucket.org/stil4m/dash-brick-feedburner.git'

    def get_available_plugins(self):
        return self.available_plugins