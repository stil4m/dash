import sys
from app.lib.dashroutesdispatcher import DashRoutesDispatcher

class Plugin:

    timeout = 60

    def __init__(self, path):
        self.name = path.split('/').pop()
        self.path = path
        module_string = "plugins."+ self.name +".brick"
        __import__(module_string)
        module =  sys.modules[module_string]

        brick_ref = module.Brick

        self.brick = brick_ref(
            DashRoutesDispatcher.shared_instance().get_dispatcher(),
            '/plugins/' + self.name,
            path)

    def get_real_name(self):
        try:
            return self.brick.get_real_name()
        except:
            return "PluginRealNameError"

    def get_name(self):
        try:
            return self.name
        except:
            return "PluginNameError"

    def update_timeout(self):
        try:
            return self.brick.timeout()
        except:
            return self.timeout

    def update(self):
        try:
            self.brick.update()
        except:
            pass

    def get_javascript(self):
        try:
            return self.brick.get_javascript()
        except:
            return None