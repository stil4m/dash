
from app.lib.singleton import Singleton
import cherrypy

@Singleton
class DashRoutesDispatcher():

    def __init__(self):
        self.dispatcher = cherrypy.dispatch.RoutesDispatcher()

    def get_dispatcher(self):
        return self.dispatcher