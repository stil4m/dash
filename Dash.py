#!/usr/bin/env python
import sys
import os
import cherrypy

#Setup Base Path
path_base = os.path.dirname(os.path.abspath(__file__))

try:
    frozen = sys.frozen
except AttributeError:
    frozen = False
    
#Setup Import Paths
sys.path.insert(0, path_base)
sys.path.insert(0, os.path.join(path_base, 'library'))

def app_already_running_on_host_and_port(host, port):
    try:
        s.connect((host, port))
        s.shutdown(0)
        return True
    except:
        return False
      
def server_start():
    
    from app.arguments.dashargs import DashArguments
    from app.models.applaunchcontext import ApplicationLaunchContext
    
    arguments =  DashArguments().parse_arguments()
    
    application_launch_context = ApplicationLaunchContext(path_base)
    
    # Create cache dir
    cache_dir = os.path.join(application_launch_context.data_dir, 'cache')
    if not os.path.isdir(cache_dir):
        os.mkdir(cache_dir)
        
    # Config app
    from app.config.applicationconfig import AppConfig
    ac = AppConfig(application_launch_context.config_file_path())
    
    debug = os.path.isfile(os.path.join(application_launch_context.data_dir, 'debug.conf'))
    
    port = int(ac.get('global', 'port'))
    host = ac.get('global', 'host')
    if app_already_running_on_host_and_port(host, port):
        return

    import app.config.render

    # User config, use own stuff to prevent unexpected results
    cherrypy.config.update({
        'global': {
            'server.thread_pool': 10,
            'server.socket_port': port,
            'server.socket_host': ac.get('global', 'host'),
            'server.environment': ac.get('global', 'server.environment'),
            'engine.autoreload_on': ac.get('global', 'server.environment') == 'development',
            'tools.mako.collection_size': 500,
            'tools.mako.directories': os.path.join(path_base, 'app', 'views'),

            'basePath': path_base,
            'runPath': path_base,
            'cachePath': cache_dir,
            'debug': debug,
            'frozen': frozen,

            # Global workers
            'config': ac,
        }
    })

    # Static config
    from app.config.dashstaticconfig import DashStaticConfig
    conf = DashStaticConfig(path_base, application_launch_context)

    from app.lib.cron import DashProcess
    from app.lib.plugins.pluginmanager import PluginManager

    plugin_manager = PluginManager.shared_instance()
    plugin_manager.update_config_with_plugins(conf)
    dp = DashProcess(cherrypy.engine, ac, debug)
    plugin_manager.setup_dash_process(dp)
    dp.subscribe()

    # Don't use auth when password is empty
    if ac.get('global', 'password') != '':
        conf['/'].update({
            'tools.basic_auth.on': True,
            'tools.basic_auth.realm': 'Awesomeness',
            'tools.basic_auth.users': {ac.get('global', 'username'):ac.get('global', 'password')},
            'tools.basic_auth.encrypt': app.clearAuthText
        })
        cherrypy.tools.mybasic_auth = cherrypy.Tool('on_start_resource', app.basicAuth)

    #No Root controller as we provided all our own.
    cherrypy.tree.mount(root = None, config = conf)

    #HTTP Errors
    def http_error_hander(status, message, traceback, version):
        args = [status, message, traceback, version]
        return "<html><body><h1>Error %s</h1>Something unexpected has happened.</body></html>" % args[0]
    cherrypy.config.update({'error_page.default' : http_error_hander})

        
    # Setup the signal handler
    if hasattr(cherrypy.engine, "signal_handler"):
        cherrypy.engine.signal_handler.set_handler(signal='SIGINT', listener=cherrypy.engine.signal_handler.bus.exit)
        cherrypy.engine.signal_handler.subscribe()
    if hasattr(cherrypy.engine, "console_control_handler"):
        cherrypy.engine.console_control_handler.subscribe()

    ## start the app
    try:
        cherrypy.engine.start()
    except:
        sys.exit(1)
    else:
        cherrypy.engine.block()

if __name__ == '__main__':
    server_start()
