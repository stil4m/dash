var dash_internal = new function() {

    var thisObject = this;
    var pluginManagerId = "pluginManager"
    this.openPluginManager = function () {
        dash.modalSettings('/pluginmanager', "dash", pluginManagerId, this.exitSettings);
        this.reloadInstalledPlugins();
    };

    this.exitSettings = function () {
        dash.hideModalSettings(pluginManagerId)
    };

    this.install_setup = function (plug_name) {
        $.get(
            '/pluginmanager/setup',
            {
                'plugin_name': plug_name
            },
            function (responseHtml) {
                $('#'+pluginManagerId).find('div.available-plugins').html(responseHtml);
            },
            'html'
        );
    };

    this.cancelInstallPlugin = function() {
        this.loadAvailablePlugins();
    }

    this.loadAvailablePlugins = function() {
        $.get(
            '/pluginmanager/availablePlugins',
            function (responseHtml) {
                $('#'+pluginManagerId).find('div.available-plugins').html(responseHtml);
            },
            'html'
        );
    };

    this.loadActivePlugins = function () {
        $.get(
            '/pluginmanager/activePlugins',
            function (responseHtml) {
                $('#'+pluginManagerId).find('div.active-plugins').html(responseHtml);
            },
            'html'
        );
    }

    this.loadInstalledPlugins = function() {
        $.get(
            '/pluginmanager/installedPlugins',
            function (responseHtml) {
                $('#'+pluginManagerId).find('div.installed-plugins').html(responseHtml);
            },
            'html'
        );
    };

    this.reloadPlugins = function () {
        $.get(
            '/plugins',
            function (responseHtml) {
                $('div.plugins').html(responseHtml)
            }
        );
    };

    this.installPlugin = function () {
        var plugin_name = $('#'+pluginManagerId).find('div.available-plugins').find("form").find("#plugin_name").val();
        var working_name = $('#'+pluginManagerId).find('div.available-plugins').find("form").find("#working_name").val();
        $.post(
            '/pluginmanager/install',
            {
                'plugin_name': plugin_name,
                'working_name': working_name
            },
            function (responseJson) {
                if (responseJson.success == true) {
                    thisObject.loadAvailablePlugins();
                    thisObject.loadInstalledPlugins();
                } else {
                    errorsElement = $("div.available-plugins > div.install-errors");
                    newHtml = '<div class="alert alert-error">';
                    newHtml += "Error: ";
                    newHtml += responseJson.errors;
                    newHtml += "</div>";
                    errorsElement.html(newHtml);
                    console.log("FAILURE")
                }
            },
            'json'
        )
    };

    this.reloadInstalledPlugins = function () {
        setTimeout(this.executeReloadInstalledPlugins, 5000);
    };

    this.executeReloadInstalledPlugins = function() {
        if ($('#'+pluginManagerId).find('div.installed-plugins').length > 0) {
            thisObject.loadInstalledPlugins();
            thisObject.reloadInstalledPlugins();
        }
    };

    this.restart = function() {
        var insert = '<div class="modal fade hide" id="restartModalView" >';
        insert += '<div class="modal-header">';
        insert += '<h3>Dash is Restarting</h3>';
        insert += '</div>';
        insert += '<div class="modal-body">';
        insert += '<p>Dash is restarting. Please wait you inpatient son of a bitch.</p>';
        insert += '<p>PS: F5-ing the fuck out of this thing will not help you a bit.</p>';
        insert += '</div>';
        insert += "</div>";
        $('body').append(insert);
        $('#restartModalView').modal({backdrop: true, keyboard: false, show: true, static: true});


        $.get(
            '/system/restart'
        );
        setTimeout(this.reloadLocationCallback, 30000)
    };

    this.reloadLocationCallback = function() {
        location.reload()
    };

    this.deletePlugin = function (pluginName) {
        $.get(
            '/pluginmanager/deletePlugin',
            {
                'plugin_name': pluginName
            },
            function () {
                thisObject.reloadPlugins();
                thisObject.loadActivePlugins();
            }
        );
    };
};
