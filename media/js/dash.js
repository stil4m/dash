var dash = new function() {

    this.render = function(path, elementID) {
        $.get(
            path,
            function(responseText){
                $("#"+elementID).html(responseText);
            },
            "html"
        );
    }

    this.modalSettings = function(path, targetElementId, settingsElementId, delegateFunction) {
        $.get(
            path,
            function(responseText) {
                var element = document.getElementById(targetElementId);
                var insert = '<div class="modal fade hide" id="';
                insert += settingsElementId;
                insert += '">';
                insert += responseText;
                insert += "</div>";
                element.innerHTML += insert;

                $('#' + settingsElementId).modal('show');
                $('#' + settingsElementId).on('hidden', function () {
                    delegateFunction()
                });
            },
            'html'
        );
    };

    this.hideModalSettings = function(element) {
        $('#' + element).modal('hide');
        $('#'+element).remove()
    };

    this.reloadSettings = function (path, elementID) {
        $.get(
            path,
            function(responseText){
                var element = $("#" + elementID);
                element.html(responseText)
            },
            "html"
        );
    };

};
